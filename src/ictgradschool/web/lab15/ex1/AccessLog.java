package ictgradschool.web.lab15.ex1;

import java.sql.Timestamp;


/////////////////////////set up java bean.

public class AccessLog {


    private int id;

    private String name;

    private String description;

    private Timestamp currenttime;



    public AccessLog(int id, String name, String description, Timestamp currenttime){
        this.id = id;
        this.name = name;
        this.description = description;
        this.currenttime = currenttime;

    }


    public AccessLog(){

    }

    public int getid() {
        return id;
    }
    public void setid(int ID) {
       id = ID;
    }


    public String getname() {
        return name;
    }
    public void setname(String Name) {
        name = Name;
    }


    public String getdescription() {
        return description;
    }
    public void setdescription(String Description) {
        description = Description;
    }


    public Timestamp getcurrenttime() {
        return currenttime;
    }
    public void setcurrenttime(Timestamp Currenttime) {
        currenttime = Currenttime;
    }



    }



