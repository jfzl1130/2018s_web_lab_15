package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve LoggingTable entries and pass them to the LoggingTableDisplay.jsp file

        PrintWriter out = response.getWriter();

        try {

            List<AccessLog> entries = getAccessLogs(out);


            request.setAttribute("entries", entries);
            request.getRequestDispatcher("LoggingTableDisplay.jsp").forward(request, response);

        } catch (SQLException ex) {

            throw new ServletException(ex);

        }




    }

    private List<AccessLog> getAccessLogs(PrintWriter out) throws SQLException { // call the Dao to get all the object information.


        try (Connection conn = DBConnection.createConnection()) {

            AccessLogDAO dao = new AccessLogDAO(conn);
            List<AccessLog> entries = dao.allAccessLogs();

            for (AccessLog entry: entries)
                System.out.println(entry.getname());

            return entries;
        }




    }





}
