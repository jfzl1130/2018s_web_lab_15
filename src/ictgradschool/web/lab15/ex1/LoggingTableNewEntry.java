package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        try (Connection conn = DBConnection.createConnection()) {

            AccessLogDAO dao = new AccessLogDAO(conn);
            String name = request.getParameter("name");
            String description = request.getParameter("description");

            AccessLog newEntry = new AccessLog();

            newEntry.setname(name);

            newEntry.setdescription(description);

            dao.addAccessLog(newEntry); // add the new entry to dao.

            response.sendRedirect("../question1"); // redirect to the same page, refresh.



            // TODO: Retrieve parameters and store new entries in the database
            // TODO: Redirect back to the LoggingTable
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);


    }
}

