

DROP TABLE IF EXISTS access_log;
CREATE  TABLE IF NOT EXISTS access_log (
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30) NOT NULL,
  description VARCHAR(50),
  currenttime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


INSERT INTO access_log (id, name,description) VALUES
  ('001', 'peter','manager'),
  ('002', 'john','staff'),
  ('003', 'Leo','staff'),
  ('004', 'Kevin','staff'),
  ('005', 'Jack','staff'),
  ('006', 'David','staff');