<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %>

<html>
<head>
    <title>Exercise 01</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
Generate a well formed table of AccessLogs here using JSTL/EL.


<form ACTION="question1/new"   <%-- url-pattern of servelet in web.xml     --%>
      METHOD="POST">
    Name:<br>
    <input type="text" name="name" ><br>
    Description:<br>
    <input type="text" name="description" ><br><br>
    <input type="submit" value="Submit">

</form>

<style>
    table#striped tr:nth-child(even) { background-color: #f2f2f2; }
</style>


<table id="striped">

<%--JSTL/EL, entries is the key from servelet,when set attribute. var just any name, the below will be name.variable.  --%>
    </tr>
    <c:forEach items="${entries}" var="AccessLog">
        <tr>

            <td>${AccessLog.id}</td>

            <td>${AccessLog.name}</td>

            <td>${AccessLog.description}</td>

            <td>${AccessLog.currenttime}</td>

        </tr>
    </c:forEach>
</table>


</body>
</html>
